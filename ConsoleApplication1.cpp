extern void test_chrono();
extern void test_strong() noexcept;
extern void test_type_with_ratio_with_longlong();
extern void test_type_with_ratio_with_strongtype();

int main()
{
	test_strong();
	test_chrono();
	test_type_with_ratio_with_longlong();
	test_type_with_ratio_with_strongtype();

	return 0;
}
