#ifndef TYPE_WITH_RATIO_H_INCLUDED_590A5F88_8E4A_4317_A582_F6B569EA07BE
#define TYPE_WITH_RATIO_H_INCLUDED_590A5F88_8E4A_4317_A582_F6B569EA07BE

#include <limits>
#include <ratio>
#include <type_traits>

#include "strong_typedef.h"

namespace avm
{
	template<class Rep, class Ratio = std::ratio<1>>
	class type_with_ratio;

	namespace detail
	{
		// STRUCT TEMPLATE treat_as_floating_point
		template<class Rep>
		struct treat_as_floating_point : std::is_floating_point<Rep>
		{	// tests for floating-point type
		};

		template<class Rep>
		/*inline*/ constexpr bool treat_as_floating_point_v = treat_as_floating_point<Rep>::value;

		// ALIAS TEMPLATE bool_constant
		template<bool Val>
		using bool_constant = std::integral_constant<bool, Val>;

		// TRAIT Is_specialization: TRUE IFF Type IS A SPECIALIZATION OF Template
		template<class Type, template <class...> class Template>
		/*inline*/ constexpr bool Is_specialization_v = false;

		template<template <class...> class Template, class... Types>
		/*inline*/ constexpr bool Is_specialization_v<Template<Types...>, Template> = true;

		template<class Type, template <class...> class Template>
		struct Is_specialization : bool_constant<Is_specialization_v<Type, Template>>
		{};

		template<class Ty>
		/*inline*/ constexpr bool Is_type_with_ratio_v = Is_specialization_v<Ty, type_with_ratio>;

		// VARIABLE TEMPLATE Is_ratio_v
		template<class Ty>
		/*inline*/ constexpr bool Is_ratio_v = false;	// test for ratio type

		template<intmax_t R1, intmax_t R2>
		/*inline*/ constexpr bool Is_ratio_v<std::ratio<R1, R2>> = true;

		// STRUCT TEMPLATE type_with_ratio_values
		template<class Rep, bool is_strong_type = type_safe::strong_typedef_op::detail::is_strong_typedef<Rep>::value>
		struct type_with_ratio_values
		{	// gets arithmetic properties of a type
			_Check_return_ static constexpr Rep zero() noexcept // Strengthened, P0972
			{	// get zero value
				return (Rep{ 0 });
			}

			_Check_return_ static constexpr Rep(min)() noexcept // Strengthened, P0972
			{	// get smallest value
				return (std::numeric_limits<Rep>::lowest());
			}

			_Check_return_ static constexpr Rep(max)() noexcept // Strengthened, P0972
			{	// get largest value
				return ((std::numeric_limits<Rep>::max)());
			}
		};

		template<class Rep>
		struct type_with_ratio_values<Rep, true>
		{
			// gets arithmetic properties of a type
			_Check_return_ static constexpr Rep zero() noexcept // Strengthened, P0972
			{	// get zero value
				return (Rep{ 0 });
			}

			_Check_return_ static constexpr Rep(min)() noexcept // Strengthened, P0972
			{	// get smallest value
				return Rep{ (std::numeric_limits<type_safe::underlying_type<Rep>>::lowest()) };
			}

			_Check_return_ static constexpr Rep(max)() noexcept // Strengthened, P0972
			{	// get largest value
				return Rep{ (std::numeric_limits<type_safe::underlying_type<Rep>>::max)() };
			}
		};

		template<intmax_t Val>
		struct Abs : std::integral_constant<intmax_t, (Val < 0 ? -Val : Val)>
		{	// computes absolute value of Val
		};

		// STRUCT TEMPLATE Gcd
		template<intmax_t Ax, intmax_t Bx>
		struct GcdX : GcdX<Bx, Ax % Bx>::type
		{	// computes greatest common divisor of Ax and Bx
		};

		template<intmax_t Ax>
		struct GcdX<Ax, 0> : std::integral_constant<intmax_t, Ax>
		{	// computes greatest common divisor of Ax and 0
		};

		template<intmax_t Ax, intmax_t Bx>
		struct Gcd : GcdX<Abs<Ax>::value, Abs<Bx>::value>::type
		{	// computes greatest common divisor of abs(Ax) and abs(Bx)
		};

		template<>
		struct Gcd<0, 0> : std::integral_constant<intmax_t, 1>	// contrary to mathematical convention
		{	// avoids division by 0 in ratio_less
		};

		// STRUCT TEMPLATE Lcm (LEAST COMMON MULTIPLE)
		template<intmax_t Ax, intmax_t Bx>
		struct Lcm : std::integral_constant<intmax_t, (Ax / Gcd<Ax, Bx>::value) * Bx>
		{	/* compute least common multiple of Ax and Bx */
		};
	}	// namespace detail

	template<class To, class Rep, class Ratio, class = std::enable_if_t<detail::Is_type_with_ratio_v<To>>>
	constexpr To type_with_ratio_cast(const type_with_ratio<Rep, Ratio>&);

	template<class Rep, class Ratio> class type_with_ratio
	{
	public:
		using rep = Rep;
		using period = typename Ratio::type;

		static_assert(!detail::Is_type_with_ratio_v<Rep>, "type_with_ratio can't have type_with_ratio as second template argument");
		static_assert(detail::Is_ratio_v<Ratio>, "period not an instance of std::ratio");
		static_assert(0 < Ratio::num, "period negative or zero");

		constexpr type_with_ratio() noexcept(noexcept(Rep{})) = default;

		template<class Rep2, class = std::enable_if_t<std::is_convertible<const Rep2&, Rep>::value && (detail::treat_as_floating_point_v<Rep> || !detail::treat_as_floating_point_v<Rep2>)>>
		constexpr explicit type_with_ratio(const Rep2& Val)
			: _MyRep(static_cast<Rep>(Val))
		{	// construct from representation
		}

		template<class Rep2, class Ratio2, class = std::enable_if_t<detail::treat_as_floating_point_v<Rep> || (std::ratio_divide<Ratio2, Ratio>::den == 1 && !detail::treat_as_floating_point_v<Rep2>)>>
		constexpr type_with_ratio(const type_with_ratio<Rep2, Ratio2>& Dur)
			: _MyRep(type_with_ratio_cast<type_with_ratio>(Dur).count())
		{	// construct from a type_with_ratio
		}

		_Check_return_ constexpr Rep count() const
		{	// get stored rep
			return (_MyRep);
		}

		_Check_return_ constexpr std::common_type_t<type_with_ratio> operator+() const
		{	// get value
			return (std::common_type_t<type_with_ratio>(*this));
		}

		_Check_return_ constexpr std::common_type_t<type_with_ratio> operator-() const
		{	// get negated value
			return (std::common_type_t<type_with_ratio>(-_MyRep));
		}

		inline type_with_ratio& operator++() noexcept(noexcept(++_MyRep))
		{	// increment rep
			++_MyRep;
			return (*this);
		}

		inline type_with_ratio operator++(int) noexcept(noexcept(_MyRep++))
		{	// postincrement rep
			return (type_with_ratio(_MyRep++));
		}

		inline type_with_ratio& operator--() noexcept(noexcept(--_MyRep))
		{	// decrement rep
			--_MyRep;
			return (*this);
		}

		inline type_with_ratio operator--(int) noexcept(noexcept(_MyRep--))
		{	// postdecrement rep
			return (type_with_ratio(_MyRep--));
		}

		inline type_with_ratio& operator+=(const type_with_ratio& Right) noexcept(noexcept(_MyRep += Right._MyRep))
		{	// add Right to rep
			_MyRep += Right._MyRep;
			return (*this);
		}

		inline type_with_ratio& operator-=(const type_with_ratio& Right) noexcept(noexcept(_MyRep -= Right._MyRep))
		{	// subtract Right from rep
			_MyRep -= Right._MyRep;
			return (*this);
		}

		inline type_with_ratio& operator*=(const Rep& Right) noexcept(noexcept(_MyRep *= Right))
		{	// multiply rep by Right
			_MyRep *= Right;
			return (*this);
		}

		inline type_with_ratio& operator/=(const Rep& Right) noexcept(noexcept(_MyRep /= Right))
		{	// divide rep by Right
			_MyRep /= Right;
			return (*this);
		}

		inline type_with_ratio& operator%=(const Rep& Right) noexcept(noexcept(_MyRep %= Right))
		{	// modulus rep by Right
			_MyRep %= Right;
			return (*this);
		}

		inline type_with_ratio& operator%=(const type_with_ratio& Right) noexcept(noexcept(_MyRep %= Right.count()))
		{	// modulus rep by Right
			_MyRep %= Right.count();
			return (*this);
		}

		_Check_return_ static constexpr type_with_ratio zero() noexcept // Strengthened, P0972
		{	// get zero value
			return (type_with_ratio(detail::type_with_ratio_values<Rep>::zero()));
		}

		_Check_return_ static constexpr type_with_ratio(min)() noexcept // Strengthened, P0972
		{	// get minimum value
			return (type_with_ratio((detail::type_with_ratio_values<Rep>::min)()));
		}

		_Check_return_ static constexpr type_with_ratio(max)() noexcept // Strengthened, P0972
		{	// get maximum value
			return (type_with_ratio((detail::type_with_ratio_values<Rep>::max)()));
		}

		// type_with_ratio COMPARISONS
		template<class Rep2, class Period2>
		_Check_return_ friend constexpr bool operator==(const type_with_ratio<Rep, Ratio>& Left, const type_with_ratio<Rep2, Period2>& Right)
		{	// test if type_with_ratio == type_with_ratio
			using CT = std::common_type_t<type_with_ratio<Rep, Ratio>, type_with_ratio<Rep2, Period2>>;
			return (CT(Left).count() == CT(Right).count());
		}

		template<class Rep2, class Period2>
		_Check_return_ friend constexpr bool operator!=(const type_with_ratio<Rep, Ratio>& Left, const type_with_ratio<Rep2, Period2>& Right)
		{	// test if type_with_ratio != type_with_ratio
			return (!(Left == Right));
		}

		template<class Rep2, class Period2>
		_Check_return_ friend constexpr bool operator<(const type_with_ratio<Rep, Ratio>& Left, const type_with_ratio<Rep2, Period2>& Right)
		{	// test if type_with_ratio < type_with_ratio
			using CT = std::common_type_t<type_with_ratio<Rep, Ratio>, type_with_ratio<Rep2, Period2>>;
			return (CT(Left).count() < CT(Right).count());
		}

		template<class Rep2, class Period2>
		_Check_return_ friend constexpr bool operator<=(const type_with_ratio<Rep, Ratio>& Left, const type_with_ratio<Rep2, Period2>& Right)
		{	// test if type_with_ratio <= type_with_ratio
			return (!(Right < Left));
		}

		template<class Rep2, class Period2>
		_Check_return_ friend constexpr bool operator>(const type_with_ratio<Rep, Ratio>& Left, const type_with_ratio<Rep2, Period2>& Right)
		{	// test if type_with_ratio > type_with_ratio
			return (Right < Left);
		}

		template<class Rep2, class Period2>
		_Check_return_ friend constexpr bool operator>=(const type_with_ratio<Rep, Ratio>& Left, const type_with_ratio<Rep2, Period2>& Right)
		{	// test if type_with_ratio >= type_with_ratio
			return (!(Left < Right));
		}

	private:
		Rep _MyRep;	// the stored rep
	};
}

namespace std
{
	template<class Rep1, class Period1, class Rep2, class Period2>
	struct common_type<avm::type_with_ratio<Rep1, Period1>, avm::type_with_ratio<Rep2, Period2>>
	{	// common type of two type_with_ratios
		using type = avm::type_with_ratio<std::common_type_t<Rep1, Rep2>, std::ratio<avm::detail::Gcd<Period1::num, Period2::num>::value, avm::detail::Lcm<Period1::den, Period2::den>::value>>;
	};
}

namespace avm
{
	// type_with_ratio ARITHMETIC
	template<class Rep1, class Period1, class Rep2, class Period2>
	_Check_return_ constexpr std::common_type_t<type_with_ratio<Rep1, Period1>, type_with_ratio<Rep2, Period2>>
		operator+(const type_with_ratio<Rep1, Period1>& Left, const type_with_ratio<Rep2, Period2>& Right)
	{	// add two type_with_ratios
		using CD = std::common_type_t<type_with_ratio<Rep1, Period1>, type_with_ratio<Rep2, Period2>>;
		return (CD(CD(Left).count() + CD(Right).count()));
	}

	template<class Rep1, class Period1, class Rep2, class Period2>
	_Check_return_ constexpr std::common_type_t<type_with_ratio<Rep1, Period1>, type_with_ratio<Rep2, Period2>>
		operator-(const type_with_ratio<Rep1, Period1>& Left, const type_with_ratio<Rep2, Period2>& Right)
	{	// subtract two type_with_ratios
		using CD = std::common_type_t<type_with_ratio<Rep1, Period1>, type_with_ratio<Rep2, Period2>>;
		return (CD(CD(Left).count() - CD(Right).count()));
	}

	template<class Rep1, class Period1, class Rep2, class = std::enable_if_t<std::is_convertible<const Rep2&, std::common_type_t<Rep1, Rep2>>::value>>
	_Check_return_ constexpr type_with_ratio<std::common_type_t<Rep1, Rep2>, Period1>
		operator*(const type_with_ratio<Rep1, Period1>& Left, const Rep2& Right)
	{	// multiply type_with_ratio by rep
		using CR = std::common_type_t<Rep1, Rep2>;
		using CD = type_with_ratio<CR, Period1>;
		return (CD(CD(Left).count() * Right));
	}

	template<class Rep1, class Rep2, class Period2, class = std::enable_if_t<std::is_convertible<const Rep1&, std::common_type_t<Rep1, Rep2>>::value>>
	_Check_return_ constexpr type_with_ratio<std::common_type_t<Rep1, Rep2>, Period2>
		operator*(const Rep1& Left, const type_with_ratio<Rep2, Period2>& Right)
	{	// multiply rep by type_with_ratio
		return (Right * Left);
	}

	namespace detail {
		template<class CR, class Period1, class Rep2, bool = std::is_convertible<const Rep2&, CR>::value>
		struct Duration_div_mod1
		{	// return type for type_with_ratio / rep and type_with_ratio % rep
			using type = type_with_ratio<CR, Period1>;
		};

		template<class CR, class Period1, class Rep2>
		struct Duration_div_mod1<CR, Period1, Rep2, false>
		{	// no return type
		};

		template<class Rep1, class Period1, class Rep2, bool = detail::Is_type_with_ratio_v<Rep2>>
		struct Duration_div_mod
		{	// no return type
		};

		template<class Rep1, class Period1, class Rep2>
		struct Duration_div_mod<Rep1, Period1, Rep2, false> : Duration_div_mod1<std::common_type_t<Rep1, Rep2>, Period1, Rep2>
		{	// return type for type_with_ratio / rep and type_with_ratio % rep
		};
	}	// namespace detail

	template<class Rep1, class Period1, class Rep2>
	_Check_return_ constexpr typename detail::Duration_div_mod<Rep1, Period1, Rep2>::type
		operator/(const type_with_ratio<Rep1, Period1>& Left, const Rep2& Right)
	{	// divide type_with_ratio by rep
		using CR = std::common_type_t<Rep1, Rep2>;
		using CD = type_with_ratio<CR, Period1>;
		return (CD(CD(Left).count() / Right));
	}

	template<class Rep1, class Period1, class Rep2, class Period2>
	_Check_return_ constexpr std::common_type_t<Rep1, Rep2>
		operator/(const type_with_ratio<Rep1, Period1>& Left, const type_with_ratio<Rep2, Period2>& Right)
	{	// divide type_with_ratio by type_with_ratio
		using CD = std::common_type_t<type_with_ratio<Rep1, Period1>, type_with_ratio<Rep2, Period2>>;
		return (CD(Left).count() / CD(Right).count());
	}

	template<class Rep1, class Period1, class Rep2>
	_Check_return_ constexpr typename detail::Duration_div_mod<Rep1, Period1, Rep2>::type
		operator%(const type_with_ratio<Rep1, Period1>& Left, const Rep2& Right)
	{	// divide type_with_ratio by rep
		using CR = std::common_type_t<Rep1, Rep2>;
		using CD = type_with_ratio<CR, Period1>;
		return (CD(CD(Left).count() % Right));
	}

	template<class Rep1, class Period1, class Rep2, class Period2>
	_Check_return_ constexpr std::common_type_t<type_with_ratio<Rep1, Period1>, type_with_ratio<Rep2, Period2>>
		operator%(const type_with_ratio<Rep1, Period1>& Left, const type_with_ratio<Rep2, Period2>& Right)
	{	// divide type_with_ratio by type_with_ratio
		using CD = std::common_type_t<type_with_ratio<Rep1, Period1>, type_with_ratio<Rep2, Period2>>;
		return (CD(CD(Left).count() % CD(Right).count()));
	}

	// FUNCTION TEMPLATE type_with_ratio_cast
	template<class To, class Rep, class Period, class Enabled>
	_Check_return_ constexpr To type_with_ratio_cast(const type_with_ratio<Rep, Period>& Dur)
	{	// convert type_with_ratio to another type_with_ratio; truncate
		using CF = std::ratio_divide<Period, typename To::period>;

		using ToRep = typename To::rep;
		//using CR = std::common_type_t<ToRep, Rep, intmax_t>;
		using CR = std::common_type_t<ToRep, Rep>;

#pragma warning(push)
#pragma warning(disable: 6326)	// Potential comparison of a constant with another constant.
		return (CF::num == 1 && CF::den == 1
			? static_cast<To>(static_cast<ToRep>(Dur.count()))
			: CF::num != 1 && CF::den == 1
			? static_cast<To>(static_cast<ToRep>(
				static_cast<CR>(
					Dur.count()) * static_cast<CR>(CF::num)))
			: CF::num == 1 && CF::den != 1
			? static_cast<To>(static_cast<ToRep>(
				static_cast<CR>(Dur.count())
				/ static_cast<CR>(CF::den)))
			: static_cast<To>(static_cast<ToRep>(
				static_cast<CR>(Dur.count()) * static_cast<CR>(CF::num)
				/ static_cast<CR>(CF::den))));
#pragma warning(pop)
	}

	// FUNCTION TEMPLATE floor
	template<class To, class Rep, class Period, class = std::enable_if_t<detail::Is_type_with_ratio_v<To>>>
	_Check_return_ constexpr To floor(const type_with_ratio<Rep, Period>& Dur)
	{	// convert type_with_ratio to another type_with_ratio; round towards negative infinity
		// i.e. the greatest integral result such that the result <= Dur
		const To Casted{ type_with_ratio_cast<To>(Dur) };
		if (Casted > Dur)
		{
			return (To{ Casted.count() - static_cast<typename To::rep>(1) });
		}

		return (Casted);
	}

	// FUNCTION TEMPLATE ceil
	template<class To, class Rep, class Period, class = std::enable_if_t<detail::Is_type_with_ratio_v<To>>>
	_Check_return_ constexpr To ceil(const type_with_ratio<Rep, Period>& Dur)
	{	// convert type_with_ratio to another type_with_ratio; round towards positive infinity
		// i.e. the least integral result such that Dur <= the result
		const To Casted{ type_with_ratio_cast<To>(Dur) };
		if (Casted < Dur)
		{
			return (To{ Casted.count() + static_cast<typename To::rep>(1) });
		}

		return (Casted);
	}

	namespace detail
	{
		// FUNCTION TEMPLATE round
		template<class Rep>
		constexpr bool Is_even(Rep Val)
		{	// Tests whether Val is even
			return (Val % 2 == 0);
		}
	}	// namespace detail

	template<class To, class Rep, class Period, class = std::enable_if_t<detail::Is_type_with_ratio_v<To> && !detail::treat_as_floating_point_v<typename To::rep>>>
	_Check_return_ constexpr To round(const type_with_ratio<Rep, Period>& Dur)
	{	// convert type_with_ratio to another type_with_ratio, round to nearest, ties to even
		const To Floored{ floor<To>(Dur) };
		const To Ceiled{ Floored + To{1} };
		const auto Floor_adjustment = Dur - Floored;
		const auto Ceil_adjustment = Ceiled - Dur;
		if (Floor_adjustment < Ceil_adjustment
			|| (Floor_adjustment == Ceil_adjustment && detail::Is_even(Floored.count())))
		{
			return (Floored);
		}

		return (Ceiled);
	}

	// FUNCTION TEMPLATE abs
	template<class Rep, class Period, class = std::enable_if_t<std::numeric_limits<Rep>::is_signed>>
	_Check_return_ constexpr type_with_ratio<Rep, Period> abs(const type_with_ratio<Rep, Period> Dur)
	{	// create a type_with_ratio with count() the absolute value of Dur.count()
		return (Dur < type_with_ratio<Rep, Period>::zero()
			? type_with_ratio<Rep, Period>::zero() - Dur
			: Dur);
	}

#if 0
	// duration LITERALS
	inline namespace literals {
		_Check_return_ constexpr chrono::hours operator "" h(unsigned long long Val)
		{	// return integral hours
			return (chrono::hours(Val));
		}

		_Check_return_ constexpr chrono::duration<double, ratio<3600>> operator "" h(
			long double Val)
		{	// return floating-point hours
			return (chrono::duration<double, ratio<3600>>(Val));
		}

		_Check_return_ constexpr chrono::minutes(operator "" min)(unsigned long long Val)
		{	// return integral minutes
			return (chrono::minutes(Val));
		}

		_Check_return_ constexpr chrono::duration<double, ratio<60>>(operator "" min)(
			long double Val)
		{	// return floating-point minutes
			return (chrono::duration<double, ratio<60>>(Val));
		}

		_Check_return_ constexpr chrono::seconds operator "" s(unsigned long long Val)
		{	// return integral seconds
			return (chrono::seconds(Val));
		}

		_Check_return_ constexpr chrono::duration<double> operator "" s(long double Val)
		{	// return floating-point seconds
			return (chrono::duration<double>(Val));
		}

		_Check_return_ constexpr chrono::milliseconds operator "" ms(unsigned long long Val)
		{	// return integral milliseconds
			return (chrono::milliseconds(Val));
		}

		_Check_return_ constexpr chrono::duration<double, milli> operator "" ms(
			long double Val)
		{	// return floating-point milliseconds
			return (chrono::duration<double, milli>(Val));
		}

		_Check_return_ constexpr chrono::microseconds operator "" us(unsigned long long Val)
		{	// return integral microseconds
			return (chrono::microseconds(Val));
		}

		_Check_return_ constexpr chrono::duration<double, micro> operator "" us(
			long double Val)
		{	// return floating-point microseconds
			return (chrono::duration<double, micro>(Val));
		}

		_Check_return_ constexpr chrono::nanoseconds operator "" ns(unsigned long long Val)
		{	// return integral nanoseconds
			return (chrono::nanoseconds(Val));
		}

		_Check_return_ constexpr chrono::duration<double, nano> operator "" ns(
			long double Val)
		{	// return floating-point nanoseconds
			return (chrono::duration<double, nano>(Val));
		}
	}	// inline namespace chrono_literals
}	// inline namespace literals
#endif
}

#endif // TYPE_WITH_RATIO_H_INCLUDED_590A5F88_8E4A_4317_A582_F6B569EA07BE
