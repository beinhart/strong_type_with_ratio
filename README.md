This project combines two concepts:
1. A type with a ratio like `std::chrono::duration`.
2. A strong type.

The strong_typedef is directly taken from [Foonathan's type_safe library](https://github.com/foonathan/type_safe) mentioned in his [blog post](https://foonathan.net/2016/10/strong-typedefs/).

The type_with_ratio is a modified copy of MSVC 2017's `std::chrono::duration`. It allows not only integral or float types but also strong types.

`ConsoleApplication1.cpp` contains example code for data rates implemented by use of `type_with_ratio`. `test_generic_datarate` uses simply an integral type
and thus comes without type safety (compiler cannot distinguish between data rate in kbps/data length in bytes/distance in meters/area in square meters).
`test_strong_datarate` uses strong types for datarate and length.