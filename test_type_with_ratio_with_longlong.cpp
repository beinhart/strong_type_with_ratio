#include <cassert>

#include "strong_typedef.h"
#include "type_with_ratio.h"

#include "test_types.h"

namespace {
	using Rep = long long;

	using kibi = std::ratio<1024, 1>;
	using mebi = std::ratio<1024 * 1024, 1>;
	using gibi = std::ratio<1024 * 1024 * 1024, 1>;

	using bps = avm::type_with_ratio<Rep>;
	using kbps = avm::type_with_ratio<Rep, std::kilo>;
	using Mbps = avm::type_with_ratio<Rep, std::mega>;
	using Gbps = avm::type_with_ratio<Rep, std::giga>;
	using Kibps = avm::type_with_ratio<Rep, kibi>;
	using Mibps = avm::type_with_ratio<Rep, mebi>;
	using Gibps = avm::type_with_ratio<Rep, gibi>;

	using Bps = avm::type_with_ratio<Rep, std::ratio<8, 1>>;
	using kBps = avm::type_with_ratio<Rep, std::ratio<8000, 1>>;
	using MBps = avm::type_with_ratio<Rep, std::ratio<8000000, 1>>;
	using GBps = avm::type_with_ratio<Rep, std::ratio<8000000000, 1>>;
	using KiBps = avm::type_with_ratio<Rep, std::ratio<8 * 1024, 1>>;
	using MiBps = avm::type_with_ratio<Rep, std::ratio<8 * 1024 * 1024, 1>>;
	using GiBps = avm::type_with_ratio<Rep, std::ratio<8ll * 1024 * 1024 * 1024, 1>>;

	using Gbps_with_smaller_rep = avm::type_with_ratio<long, std::giga>;		// Special case: smaller represensation

	inline namespace literals {
		constexpr bps operator "" _bps(unsigned long long Val) { return bps{ Val }; }
		constexpr kbps operator "" _kbps(unsigned long long Val) { return kbps{ Val }; }
		constexpr Mbps operator "" _Mbps(unsigned long long Val) { return Mbps{ Val }; }
		constexpr Gbps operator "" _Gbps(unsigned long long Val) { return Gbps{ Val }; }
		constexpr Kibps operator "" _Kibps(unsigned long long Val) { return Kibps{ Val }; }
		constexpr Mibps operator "" _Mibps(unsigned long long Val) { return Mibps{ Val }; }
		constexpr Gibps operator "" _Gibps(unsigned long long Val) { return Gibps{ Val }; }

		constexpr Bps operator "" _Bps(unsigned long long Val) { return Bps{ Val }; }
		constexpr kBps operator "" _kBps(unsigned long long Val) { return kBps{ Val }; }
		constexpr MBps operator "" _MBps(unsigned long long Val) { return MBps{ Val }; }
		constexpr GBps operator "" _GBps(unsigned long long Val) { return GBps{ Val }; }
		constexpr KiBps operator "" _KiBps(unsigned long long Val) { return KiBps{ Val }; }
		constexpr MiBps operator "" _MiBps(unsigned long long Val) { return MiBps{ Val }; }
		constexpr GiBps operator "" _GiBps(unsigned long long Val) { return GiBps{ Val }; }
	}
}

void test_type_with_ratio_with_longlong()
{
	// Constructors
	const auto s0 = GBps{ 7ll };
	assert(s0.min().count() <= s0.count());
	assert(s0.count() <= s0.max().count());
	assert(s0.count() == 7ll);
	const auto s1 = GBps{ s0 };
	assert(s1.count() == s0.count());
	const auto s2 = MBps{ s0 };
	assert(s2.count() == s0.count() * 1000);
	const auto s3 = kBps{ s0 };
	assert(s3.count() == s0.count() * 1000000);
	const auto s4 = Bps{ s0 };
	assert(s4.count() == s0.count() * 1000000000);
	const auto s5 = Gbps{ s0 };
	assert(s5.count() == s0.count() * 8);
	const auto s6 = Mbps{ s0 };
	assert(s6.count() == s0.count() * 8000);
	const auto s7 = kbps{ s0 };
	assert(s7.count() == s0.count() * 8000000);
	const auto s8 = bps{ s0 };
	assert(s8.count() == s0.count() * 8000000000);

	// Cast
	avm::type_with_ratio<Rep>(42ll);
	const auto r0 = bps{ Rep{ 16000000000ll } };
	assert(r0.count() == 16000000000ll);
	const auto r1 = bps{ r0 };
	assert(r1.count() == r0.count());
	const auto r2 = avm::type_with_ratio_cast<kbps>(r0);
	assert(r2.count() == r0.count() / 1000);
	const auto r3 = avm::type_with_ratio_cast<Mbps>(r0);
	assert(r3.count() == r0.count() / 1000000);
	const auto r4 = avm::type_with_ratio_cast<Gbps>(r0);
	assert(r4.count() == r0.count() / 1000000000);
	const auto r5 = avm::type_with_ratio_cast<Bps>(r0);
	assert(r5.count() == r0.count() / 8);
	const auto r6 = avm::type_with_ratio_cast<kBps>(r0);
	assert(r6.count() == r0.count() / 8000);
	const auto r7 = avm::type_with_ratio_cast<MBps>(r0);
	assert(r7.count() == r0.count() / 8000000);
	const auto r8 = avm::type_with_ratio_cast<GBps>(r0);
	assert(r8.count() == r0.count() / 8000000000);
	const auto r9 = avm::type_with_ratio_cast<Gbps_with_smaller_rep>(r0);
	assert(r8.count() == r0.count() / 8000000000);

	// Comparison
	assert(bps().count() == 0ll);
	assert(bps() == bps::zero());
	assert(r7 == r8);
	assert(!(r7 != r8));
	assert(!(r7 < r8));
	assert(r7 <= r8);
	assert(!(r7 > r8));
	assert(r7 >= r8);
	assert(r8 == r7);
	assert(!(r8 != r7));
	assert(!(r8 < r7));
	assert(r8 <= r7);
	assert(!(r8 > r7));
	assert(r8 >= r7);

	// Unary + and -
	assert(r7 == +r7);
	assert(r7.count() == -(-r7).count());
	assert(r7 == -(-r7));

	// Comparison in real code
	const kbps phy_rate_tx{};
	if (phy_rate_tx > kbps{ 100ll }) {}
	if (phy_rate_tx > Mbps{ 100ll }) {}
	if (phy_rate_tx > Gbps_with_smaller_rep{ 10 }) {}
	if (phy_rate_tx > 100_kbps) {}
	if (phy_rate_tx > 100_Mbps) {}
	if (phy_rate_tx.count() > 100ll) {}
	if (phy_rate_tx.count() > 100) {}

	bps x{ 42ll };
	assert(x.count() == 42ll);

	// Assignment
	auto scopy = x;
	scopy = x;

	// Increment
	x = bps{ 42ll };
	assert((++x).count() == 43ll);
	assert(x.count() == 43ll);
	assert((x++).count() == 43ll);
	assert(x.count() == 44ll);

	// Decrement
	x = bps{ 44ll };
	assert((--x).count() == 43ll);
	assert(x.count() == 43ll);
	assert((x--).count() == 43ll);
	assert(x.count() == 42ll);

	// Addition
	x = bps{ 42ll };
	x += bps{ 3ll };
	assert(x.count() == 45ll);
	x += kbps{ 3ll };
	assert(x.count() == 3045ll);
	assert(bps{ 2ll } +kbps{ 3ll } == bps{ 3002ll });

	// Subtraction
	x = bps{ 3042ll };
	x -= bps{ 3ll };
	assert(x.count() == 3039ll);
	x -= kbps{ 3ll };
	assert(x.count() == 39ll);
	assert(kbps{ 2ll } -bps{ 3ll } == bps{ 1997ll });

	// Multiplication
	x = bps{ 42ll };
	// TODO wollen wir diesen Operator haben?
	x *= 10ll;
	assert(x.count() == 420ll);
	// TODO wollen wir diesen Operator haben?
	x *= 10;
	assert(x.count() == 4200ll);
	// TODO wollen wir diesen Operator haben?
	assert(bps{ 2ll } *3ll == bps{ 6ll });
	// TODO wollen wir diesen Operator haben?
	assert(2ll *bps{ 3ll } == bps{ 6ll });
	// TODO wollen wir diesen Operator haben?
	assert(bps{ 2ll } *3 == bps{ 6ll });
	// TODO wollen wir diesen Operator haben?
	assert(3 * bps{ 2ll } == bps{ 6ll });

	// Division
	x = bps{ 4200ll };
	// TODO wollen wir diesen Operator haben?
	x /= 10ll;
	assert(x.count() == 420ll);
	// TODO wollen wir diesen Operator haben?
	x /= 10;
	assert(x.count() == 42ll);
	// TODO wollen wir diesen Operator haben? R�ckgabetyp?
	assert(bps{ 6ll } / bps{ 3ll } == 2ll);
	// TODO wollen wir diesen Operator haben? R�ckgabetyp?
	assert(bps{ 6ll } / bps{ 3ll } == 2);
	// TODO wollen wir diesen Operator haben?
	assert(bps{ 6ll } / 3ll == bps{ 2ll });
	// TODO wollen wir diesen Operator haben?
	assert(bps{ 6ll } / 3 == bps{ 2ll });

	// Modulo
	x = bps{ 1234ll };
	// TODO wollen wir diesen Operator haben?
	x %= 1000ll;
	assert(x.count() == 234ll);
	// TODO wollen wir diesen Operator haben?
	x %= 100;
	assert(x.count() == 34ll);
	// TODO wollen wir diesen Operator haben?
	x %= bps{ 10ll };
	assert(x.count() == 4ll);
	// TODO wollen wir diesen Operator haben? R�ckgabetyp?
	assert(bps{ 6ll } % bps{ 4ll } == bps{ 2ll });
#if 0	// compiliert nicht
	// TODO wollen wir diesen Operator haben? R�ckgabetyp?
	assert(bps{ 6ll } % bps{ 4ll } == 2ll);
	// TODO wollen wir diesen Operator haben? R�ckgabetyp?
	assert(bps{ 6ll } % bps{ 4ll } == 2);
#endif
	// TODO wollen wir diesen Operator haben? R�ckgabetyp?
	assert(bps{ 6ll } % 4ll == bps{ 2ll });
#if 0	// compiliert nicht
	// TODO wollen wir diesen Operator haben? R�ckgabetyp?
	assert(bps{ 6ll } % 4ll == 2ll);
	// TODO wollen wir diesen Operator haben? R�ckgabetyp?
	assert(bps{ 6ll } % 4 == 2);
#endif
	// TODO wollen wir diesen Operator haben? R�ckgabetyp?
	assert(kbps{ 6ll } % bps{ 4000ll } == kbps{ 2ll });
#if 0	// compiliert nicht
	// TODO wollen wir diesen Operator haben? R�ckgabetyp?
	assert(kbps{ 6ll } % bps{ 4000ll } == 2000ll);
	// TODO wollen wir diesen Operator haben? R�ckgabetyp?
	assert(kbps{ 6ll } % bps{ 4000ll } == 2000);
#endif
	// TODO wollen wir diesen Operator haben? R�ckgabetyp?
	assert(bps{ 6ll } % 4ll == bps{ 2ll });
#if 0	// compiliert nicht
	// TODO wollen wir diesen Operator haben? R�ckgabetyp?
	assert(bps{ 6ll } % 4ll == 2ll);
	// TODO wollen wir diesen Operator haben? R�ckgabetyp?
	assert(bps{ 6ll } % 4 == 2);
#endif

#if 0	// This code must not compile
	// Loosing precision, not allowed
	kbps{ bps{} };
#endif

	{
		using meter = avm::type_with_ratio<Rep>;

#if 1	// This code compiles, Conversion between different (not strong) Reps, can't be checked
		static_cast<void>(avm::type_with_ratio_cast<meter>(bps{}));
		static_cast<void>(bps{} == meter{});
#endif
	}

	// TODO: check floor, ceil, round, abs
}
