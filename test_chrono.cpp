#include <cassert>
#include <chrono>

using namespace std::chrono;
using namespace std::chrono_literals;

void test_chrono()
{
	// Constructors
	const auto s0 = hours{ 7 };
	assert(s0.min().count() <= s0.count());
	assert(s0.count() <= s0.max().count());
	assert(s0.count() == 7);
	const auto s1 = hours{ s0 };
	assert(s1.count() == s0.count());
	const auto s2 = minutes{ s0 };
	assert(s2.count() == s0.count() * 60);
	const auto s3 = seconds{ s0 };
	assert(s3.count() == s0.count() * 60 * 60);

	// Cast
	seconds(42);
	const auto r0 = seconds{ 24 * 60 * 60 };
	assert(r0.count() == 24 * 60 * 60);
	const auto r1 = seconds{ r0 };
	assert(r1.count() == r0.count());
	const auto r2 = duration_cast<minutes>(r0);
	assert(r2.count() == r0.count() / 60);
	const auto r3 = duration_cast<hours>(r0);
	assert(r3.count() == r0.count() / 60 / 60);

	// Comparison
	assert(seconds().count() == 0);
	assert(seconds() == seconds::zero());
	assert(r0 == r1);
	assert(!(r0 != r1));
	assert(!(r0 < r1));
	assert(r0 <= r1);
	assert(!(r0 > r1));
	assert(r0 >= r1);
	assert(r1 == r0);
	assert(!(r1 != r0));
	assert(!(r1 < r0));
	assert(r1 <= r0);
	assert(!(r1 > r0));
	assert(r1 >= r0);

	// Unary + and -
	assert(r0 == +r0);
	assert(r0.count() == -(-r0).count());
	assert(r0 == -(-r0));

	// Comparison in real code
	const hours phy_rate_tx{};
	if (phy_rate_tx > minutes{ 100 }) {}
	if (phy_rate_tx > seconds{ 100 }) {}
	if (phy_rate_tx > duration<long long>{ 10 }) {}
	if (phy_rate_tx > duration<int>{ 10 }) {}
	if (phy_rate_tx > duration<short>{ 10 }) {}
	if (phy_rate_tx > 100min) {}
	if (phy_rate_tx > 100s) {}
	if (phy_rate_tx.count() > 100) {}

	seconds x{ 42 };
	assert(x.count() == 42);

	// Assignment
	auto scopy = x;
	scopy = x;

	// Increment
	x = seconds{ 42 };
	assert((++x).count() == 43);
	assert(x.count() == 43);
	assert((x++).count() == 43);
	assert(x.count() == 44);

	// Decrement
	x = seconds{ 44 };
	assert((--x).count() == 43);
	assert(x.count() == 43);
	assert((x--).count() == 43);
	assert(x.count() == 42);

	// Addition
	x = seconds{ 42ll };
	x += seconds{ 3ll };
	assert(x.count() == 45ll);
	x += minutes{ 3ll };
	assert(x.count() == 225ll);
	assert(seconds{ 2ll } + minutes{ 3ll } == seconds{ 182ll });

	// Subtraction
	x = seconds{ 3042ll };
	x -= seconds{ 3ll };
	assert(x.count() == 3039ll);
	x -= minutes{ 3ll };
	assert(x.count() == 2859ll);
	assert(minutes{ 2ll } - seconds{ 3ll } == seconds{ 117ll });

	// Multiplication
	x = seconds{ 42ll };
	// TODO wollen wir diesen Operator haben?
	x *= 10ll;
	assert(x.count() == 420ll);
	// TODO wollen wir diesen Operator haben?
	x *= 10;
	assert(x.count() == 4200ll);
	// TODO wollen wir diesen Operator haben?
	assert(seconds{ 2ll } *3ll == seconds{ 6ll });
	// TODO wollen wir diesen Operator haben?
	assert(2ll *seconds{ 3ll } == seconds{ 6ll });
	// TODO wollen wir diesen Operator haben?
	assert(seconds{ 2ll } *3 == seconds{ 6ll });
	// TODO wollen wir diesen Operator haben?
	assert(3 * seconds{ 2ll } == seconds{ 6ll });

	// Division
	x = seconds{ 4200ll };
	// TODO wollen wir diesen Operator haben?
	x /= 10ll;
	assert(x.count() == 420ll);
	// TODO wollen wir diesen Operator haben?
	x /= 10;
	assert(x.count() == 42ll);
	// TODO wollen wir diesen Operator haben? R�ckgabetyp?
	assert(seconds{ 6ll } / seconds{ 3ll } == 2ll);
	// TODO wollen wir diesen Operator haben? R�ckgabetyp?
	assert(seconds{ 6ll } / seconds{ 3ll } == 2);
	// TODO wollen wir diesen Operator haben?
	assert(seconds{ 6ll } / 3ll == seconds{ 2ll });
	// TODO wollen wir diesen Operator haben?
	assert(seconds{ 6ll } / 3 == seconds{ 2ll });

	// Modulo
	x = seconds{ 1234ll };
	// TODO wollen wir diesen Operator haben?
	x %= 1000ll;
	assert(x.count() == 234ll);
	// TODO wollen wir diesen Operator haben?
	x %= 100;
	assert(x.count() == 34ll);
	// TODO wollen wir diesen Operator haben?
	x %= seconds{ 10ll };
	assert(x.count() == 4ll);
	// TODO wollen wir diesen Operator haben? R�ckgabetyp?
	assert(seconds{ 6ll } % seconds{ 4ll } == seconds{ 2ll });
#if 0	// compiliert nicht
	// TODO wollen wir diesen Operator haben? R�ckgabetyp?
	assert(seconds{ 6ll } % seconds{ 4ll } == 2ll);
	// TODO wollen wir diesen Operator haben? R�ckgabetyp?
	assert(seconds{ 6ll } % seconds{ 4ll } == 2);
#endif
	// TODO wollen wir diesen Operator haben? R�ckgabetyp?
	assert(seconds{ 6ll } % 4ll == seconds{ 2ll });
#if 0	// compiliert nicht
	// TODO wollen wir diesen Operator haben? R�ckgabetyp?
	assert(seconds{ 6ll } % 4ll == 2ll);
	// TODO wollen wir diesen Operator haben? R�ckgabetyp?
	assert(seconds{ 6ll } % 4 == 2);
#endif
	// TODO wollen wir diesen Operator haben? R�ckgabetyp?
	assert(minutes{ 6ll } % seconds{ 41ll } == seconds{ 32ll });
#if 0	// compiliert nicht
	// TODO wollen wir diesen Operator haben? R�ckgabetyp?
	assert(minutes{ 6ll } % seconds{ 4000ll } == 2000ll);
	// TODO wollen wir diesen Operator haben? R�ckgabetyp?
	assert(minutes{ 6ll } % seconds{ 4000ll } == 2000);
#endif
	// TODO wollen wir diesen Operator haben? R�ckgabetyp?
	assert(seconds{ 6ll } % 4ll == seconds{ 2ll });
#if 0	// compiliert nicht
	// TODO wollen wir diesen Operator haben? R�ckgabetyp?
	assert(seconds{ 6ll } % 4ll == 2ll);
	// TODO wollen wir diesen Operator haben? R�ckgabetyp?
	assert(seconds{ 6ll } % 4 == 2);
#endif

#if 0	// This code must not compile
	// Loosing precision, not allowed
	minutes{ seconds{} };
#endif

	// TODO: check floor, ceil, round, abs
}
