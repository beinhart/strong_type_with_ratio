#pragma once

#include "strong_typedef.h"

template<class T>
struct datarate
	: type_safe::strong_typedef<datarate<T>, T>
	, type_safe::strong_typedef_op::unary_plus<datarate<T>>
	, type_safe::strong_typedef_op::unary_minus<datarate<T>>
	, type_safe::strong_typedef_op::addition<datarate<T>>
	, type_safe::strong_typedef_op::subtraction<datarate<T>>
	, type_safe::strong_typedef_op::multiplication<datarate<T>>
	, type_safe::strong_typedef_op::mixed_multiplication<datarate<T>, T>
	, type_safe::strong_typedef_op::division<datarate<T>>
	, type_safe::strong_typedef_op::mixed_division<datarate<T>, T>
	, type_safe::strong_typedef_op::modulo<datarate<T>>
	, type_safe::strong_typedef_op::mixed_modulo<datarate<T>, T>
	, type_safe::strong_typedef_op::equality_comparison<datarate<T>>
	, type_safe::strong_typedef_op::relational_comparison<datarate<T>>
	, type_safe::strong_typedef_op::increment<datarate<T>>
	, type_safe::strong_typedef_op::decrement<datarate<T>>
{
	using strong_typedef::strong_typedef;
};

template<class T>
struct length : type_safe::strong_typedef<length<T>, T>
	, type_safe::strong_typedef_op::unary_plus<length<T>>
	, type_safe::strong_typedef_op::unary_minus<length<T>>
	, type_safe::strong_typedef_op::addition<length<T>>
	, type_safe::strong_typedef_op::subtraction<length<T>>
	, type_safe::strong_typedef_op::mixed_multiplication<length<T>, T>
	, type_safe::strong_typedef_op::mixed_division<length<T>, T>
	, type_safe::strong_typedef_op::mixed_modulo<length<T>, T>
	, type_safe::strong_typedef_op::equality_comparison<length<T>>
	, type_safe::strong_typedef_op::relational_comparison<length<T>>
	, type_safe::strong_typedef_op::increment<length<T>>
	, type_safe::strong_typedef_op::decrement<length<T>>
{
	using strong_typedef::strong_typedef;
};
