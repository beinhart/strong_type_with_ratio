#include "test_types.h"

void test_strong() noexcept
{
	datarate<int> d{ 42 };
	static_cast<int&>(d) = 3;
	if (d == datarate<int>(3)) {
	}
	if (static_cast<int&>(d) == 3) {
	}

	length<int> l{ 3 };
	static_cast<int&>(l) = 42;
	if (l == length<int>(3)) {
	}
	if (static_cast<int&>(l) == 3) {
	}

#if 0	// This code must not compile
	// Conversion between different strong Reps, not allowed
	if (d == length<int>(3)) {
	}
	if (l == datarate<int>(3)) {
	}
	if (d == l) {
	}
#endif
}
