#include <cassert>

#include "strong_typedef.h"
#include "type_with_ratio.h"

#include "test_types.h"

namespace {
	using Rep = datarate<long long>;

	using kibi = std::ratio<1024, 1>;
	using mebi = std::ratio<1024 * 1024, 1>;
	using gibi = std::ratio<1024 * 1024 * 1024, 1>;

	using bps = avm::type_with_ratio<Rep>;
	using kbps = avm::type_with_ratio<Rep, std::kilo>;
	using Mbps = avm::type_with_ratio<Rep, std::mega>;
	using Gbps = avm::type_with_ratio<Rep, std::giga>;
	using Kibps = avm::type_with_ratio<Rep, kibi>;
	using Mibps = avm::type_with_ratio<Rep, mebi>;
	using Gibps = avm::type_with_ratio<Rep, gibi>;

	using Bps = avm::type_with_ratio<Rep, std::ratio<8, 1>>;
	using kBps = avm::type_with_ratio<Rep, std::ratio<8000, 1>>;
	using MBps = avm::type_with_ratio<Rep, std::ratio<8000000, 1>>;
	using GBps = avm::type_with_ratio<Rep, std::ratio<8000000000, 1>>;
	using KiBps = avm::type_with_ratio<Rep, std::ratio<8 * 1024, 1>>;
	using MiBps = avm::type_with_ratio<Rep, std::ratio<8 * 1024 * 1024, 1>>;
	using GiBps = avm::type_with_ratio<Rep, std::ratio<8ll * 1024 * 1024 * 1024, 1>>;

	using Gbps_with_smaller_rep = avm::type_with_ratio<datarate<long>, std::giga>;		// Special case: smaller represensation

	inline namespace literals {
		// static_cast is a quick hack, might cause trouble.
		constexpr bps operator "" _bps(unsigned long long Val) { return bps{ static_cast<Rep>(Val) }; }
		constexpr kbps operator "" _kbps(unsigned long long Val) { return kbps{ static_cast<Rep>(Val) }; }
		constexpr Mbps operator "" _Mbps(unsigned long long Val) { return Mbps{ static_cast<Rep>(Val) }; }
		constexpr Gbps operator "" _Gbps(unsigned long long Val) { return Gbps{ static_cast<Rep>(Val) }; }
		constexpr Kibps operator "" _Kibps(unsigned long long Val) { return Kibps{ static_cast<Rep>(Val) }; }
		constexpr Mibps operator "" _Mibps(unsigned long long Val) { return Mibps{ static_cast<Rep>(Val) }; }
		constexpr Gibps operator "" _Gibps(unsigned long long Val) { return Gibps{ static_cast<Rep>(Val) }; }

		constexpr Bps operator "" _Bps(unsigned long long Val) { return Bps{ static_cast<Rep>(Val) }; }
		constexpr kBps operator "" _kBps(unsigned long long Val) { return kBps{ static_cast<Rep>(Val) }; }
		constexpr MBps operator "" _MBps(unsigned long long Val) { return MBps{ static_cast<Rep>(Val) }; }
		constexpr GBps operator "" _GBps(unsigned long long Val) { return GBps{ static_cast<Rep>(Val) }; }
		constexpr KiBps operator "" _KiBps(unsigned long long Val) { return KiBps{ static_cast<Rep>(Val) }; }
		constexpr MiBps operator "" _MiBps(unsigned long long Val) { return MiBps{ static_cast<Rep>(Val) }; }
		constexpr GiBps operator "" _GiBps(unsigned long long Val) { return GiBps{ static_cast<Rep>(Val) }; }
	}
}

void test_type_with_ratio_with_strongtype()
{
	// Constructors
	const auto s0 = GBps{ Rep{ 7ll } };
	assert(s0.min().count() <= s0.count());
	assert(s0.count() <= s0.max().count());
	assert(s0.count() == Rep{ 7 });
	const auto s1 = GBps{ s0 };
	assert(s1.count() == s0.count());
	const auto s2 = MBps{ s0 };
	assert(s2.count() == s0.count() * 1000);
	const auto s3 = kBps{ s0 };
	assert(s3.count() == s0.count() * 1000000);
	const auto s4 = Bps{ s0 };
	assert(s4.count() == s0.count() * 1000000000);
	const auto s5 = Gbps{ s0 };
	assert(s5.count() == s0.count() * 8);
	const auto s6 = Mbps{ s0 };
	assert(s6.count() == s0.count() * 8000);
	const auto s7 = kbps{ s0 };
	assert(s7.count() == s0.count() * 8000000);
	const auto s8 = bps{ s0 };
	assert(s8.count() == s0.count() * 8000000000);

	// Cast
	avm::type_with_ratio<Rep>(Rep{ 42 });
	const auto r0 = bps{ Rep{ 16000000000ll } };
	assert(r0.count() == Rep{ 16000000000 });
	const auto r1 = bps{ r0 };
	assert(r1.count() == r0.count());
	const auto r2 = avm::type_with_ratio_cast<kbps>(r0);
	assert(r2.count() == r0.count() / 1000);
	const auto r3 = avm::type_with_ratio_cast<Mbps>(r0);
	assert(r3.count() == r0.count() / 1000000);
	const auto r4 = avm::type_with_ratio_cast<Gbps>(r0);
	assert(r4.count() == r0.count() / 1000000000);
	const auto r5 = avm::type_with_ratio_cast<Bps>(r0);
	assert(r5.count() == r0.count() / 8);
	const auto r6 = avm::type_with_ratio_cast<kBps>(r0);
	assert(r6.count() == r0.count() / 8000);
	const auto r7 = avm::type_with_ratio_cast<MBps>(r0);
	assert(r7.count() == r0.count() / 8000000);
	const auto r8 = avm::type_with_ratio_cast<GBps>(r0);
	assert(r8.count() == r0.count() / 8000000000);
#if 0	// compiliert nicht
	const auto r9 = avm::type_with_ratio_cast<Gbps_with_smaller_rep>(r0);
	assert(r8.count() == r0.count() / 8000000000);
#endif

	// Comparison
	assert(bps().count() == Rep{ 0 });
	assert(bps() == bps::zero());
	assert(r7 == r8);
	assert(!(r7 != r8));
	assert(!(r7 < r8));
	assert(r7 <= r8);
	assert(!(r7 > r8));
	assert(r7 >= r8);
	assert(r8 == r7);
	assert(!(r8 != r7));
	assert(!(r8 < r7));
	assert(r8 <= r7);
	assert(!(r8 > r7));
	assert(r8 >= r7);

	// Unary + and -
	assert(r7 == +r7);
	assert(r7.count() == -(-r7).count());
	assert(r7 == -(-r7));

	// Comparison in real code
	const kbps phy_rate_tx{};
	if (phy_rate_tx > kbps{ Rep{ 100 } }) {}
	if (phy_rate_tx > Mbps{ Rep{ 100 } }) {}
#if 0	// compiliert nicht, sollte es aber? (WHE)
	if (phy_rate_tx > Gbps_with_smaller_rep{ datarate<long>{ 10 } }) {}
#endif
	if (phy_rate_tx > 100_kbps) {}
	if (phy_rate_tx > 100_Mbps) {}
	if (phy_rate_tx.count() > Rep{ 100 }) {}
	if (get(phy_rate_tx.count()) > 100) {}
	// TODO Allow this?
	//if (phy_rate_tx.count() > 100){}

	bps x{ Rep{ 42 } };
	assert(x.count() == Rep{ 42 });

	// Assignment
	auto scopy = x;
	scopy = x;

	// Increment
	x = bps{ Rep{ 42 } };
	assert((++x).count() == Rep{ 43 });
	assert(x.count() == Rep{ 43 });
	assert((x++).count() == Rep{ 43 });
	assert(x.count() == Rep{ 44 });

	// Decrement
	x = bps{ Rep{ 44 } };
	assert((--x).count() == Rep{ 43 });
	assert(x.count() == Rep{ 43 });
	assert((x--).count() == Rep{ 43 });
	assert(x.count() == Rep{ 42 });

	// Addition
	x = bps{ Rep{ 42 } };
	x += bps{ Rep{ 3 } };
	assert(x.count() == Rep{ 45 });
	x += kbps{ Rep{ 3 } };
	assert(x.count() == Rep{ 3045 });
	assert(bps{ Rep{ 2 } } +kbps{ Rep{ 3 } } == bps{ Rep{ 3002 } });

	// Subtraction
	x = bps{ Rep{ 3042 } };
	x -= bps{ Rep{ 3 } };
	assert(x.count() == Rep{ 3039 });
	x -= kbps{ Rep{ 3 } };
	assert(x.count() == Rep{ 39 });
	assert(kbps{ Rep{ 2 } } -bps{ Rep{ 3 } } == bps{ Rep{ 1997 } });

	// Multiplication
	x = bps{ Rep{ 42 } };
	// TODO wollen wir diesen Operator haben?
	x *= Rep{ 10 };
	assert(x.count() == Rep{ 420 });
#if 0	// compiliert nicht
	// TODO wollen wir diesen Operator haben?
	x *= 10;
	assert(x.count() == Rep{ 4200 });
#endif
	// TODO wollen wir diesen Operator haben?
	assert(bps{ Rep{ 2 } } *Rep{ 3 } == bps{ Rep{ 6 } });
	// TODO wollen wir diesen Operator haben?
	assert(Rep{ 2 } *bps{ Rep{ 3 } } == bps{ Rep{ 6 } });
#if 0	// compiliert nicht
	// TODO wollen wir diesen Operator haben?
	assert(bps{ Rep{ 2 } } *3 == bps{ Rep{ 6 } });
	// TODO wollen wir diesen Operator haben?
	assert(3 * bps{ Rep{ 2 } } == bps{ Rep{ 6 } });
#endif

	// Division
	x = bps{ Rep{ 4200 } };
	// TODO wollen wir diesen Operator haben?
	x /= Rep{ 10 };
	assert(x.count() == Rep{ 420 });
#if 0	// compiliert nicht
	// TODO wollen wir diesen Operator haben?
	x /= 10;
	assert(x.count() == Rep{ 42 });
#endif
	// TODO wollen wir diesen Operator haben? R�ckgabetyp?
	assert(bps{ Rep{ 6 } } / bps{ Rep{ 3 } } == Rep{ 2 });
#if 0	// compiliert nicht
	// TODO wollen wir diesen Operator haben? R�ckgabetyp?
	assert(bps{ Rep{ 6 } } / bps{ Rep{ 3 } } == 2);
#endif
	// TODO wollen wir diesen Operator haben?
	assert(bps{ Rep{ 6 } } / Rep{ 3 } == bps{ Rep{ 2 } });
#if 0	// compiliert nicht
	// TODO wollen wir diesen Operator haben?
	assert(bps{ Rep{ 6 } } / 3 == bps{ Rep{ 2 } });
#endif

	// Modulo
	x = bps{ Rep{ 1234 } };
	// TODO wollen wir diesen Operator haben?
	x %= Rep{ 1000 };
	assert(x.count() == Rep{ 234 });
#if 0	// compiliert nicht
	// TODO wollen wir diesen Operator haben?
	x %= 100;
	assert(x.count() == Rep{ 34 });
#endif
	// TODO wollen wir diesen Operator haben?
	x %= bps{ Rep{ 10 } };
	assert(x.count() == Rep{ 4 });
	// TODO wollen wir diesen Operator haben? R�ckgabetyp?
	assert(bps{ Rep{ 6 } } % bps{ Rep{ 4 } } == bps{ Rep{ 2 } });
#if 0	// compiliert nicht
	// TODO wollen wir diesen Operator haben? R�ckgabetyp?
	assert(bps{ Rep{ 6 } } % bps{ Rep{ 4 } } == Rep{ 2 });
	// TODO wollen wir diesen Operator haben? R�ckgabetyp?
	assert(bps{ Rep{ 6 } } % bps{ Rep{ 4 } } == 2);
#endif
	// TODO wollen wir diesen Operator haben? R�ckgabetyp?
	assert(bps{ Rep{ 6 } } % Rep{ 4 } == bps{ Rep{ 2 } });
#if 0	// compiliert nicht
	// TODO wollen wir diesen Operator haben? R�ckgabetyp?
	assert(bps{ Rep{ 6 } } % Rep{ 4 } == Rep{ 2 });
	// TODO wollen wir diesen Operator haben? R�ckgabetyp?
	assert(bps{ Rep{ 6 } } % 4 == 2);
#endif
	// TODO wollen wir diesen Operator haben? R�ckgabetyp?
	assert(kbps{ Rep{ 6 } } % bps{ Rep{ 4000 } } == kbps{ Rep{ 2 } });
#if 0	// compiliert nicht
	// TODO wollen wir diesen Operator haben? R�ckgabetyp?
	assert(kbps{ Rep{ 6 } } % bps{ Rep{ 4000 } } == Rep{ 2000 });
	// TODO wollen wir diesen Operator haben? R�ckgabetyp?
	assert(kbps{ Rep{ 6 } } % bps{ Rep{ 4000 } } == 2000);
#endif
	// TODO wollen wir diesen Operator haben? R�ckgabetyp?
	assert(bps{ Rep{ 6 } } % bps{ Rep{ 4 } } == bps{ Rep{ 2 } });
#if 0	// compiliert nicht
	// TODO wollen wir diesen Operator haben? R�ckgabetyp?
	assert(bps{ Rep{ 6 } } % bps{ Rep{ 4 } } == Rep{ 2 });
	// TODO wollen wir diesen Operator haben? R�ckgabetyp?
	assert(bps{ Rep{ 6 } } % bps{ Rep{ 4 } } == 2);
#endif

#if 0	// This code must not compile
	// Loosing precision, not allowed
	kbps{ bps{} };
#endif

	{
		using meter = length<int>;

#if 0	// This code must not compile, Conversion between different strong Reps is not allowed
		static_cast<void>(avm::type_with_ratio_cast<meter>(bps{}));
		static_cast<void>(bps{} == meter{});
#endif
	}
}
